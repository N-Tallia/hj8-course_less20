  var BookApp = angular.module('BookApp', ["ngRoute"])
    .config(function($routeProvider){
        $routeProvider.when('/all-book',
        {
            templateUrl:'books/book-list.html',
            controller:'BookListCtrl'
        });
        $routeProvider.when('/one-book',
        {
            templateUrl:'books/book-one.html',
            controller:'BookOneCtrl'
        });
        $routeProvider.otherwise({redirectTo: '/all-book'});
});
